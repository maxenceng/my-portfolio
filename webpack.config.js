const HtmlWebpackPlugin = require('html-webpack-plugin')

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
    template: './app/views/index.html',
    filename: 'index.html',
    inject: 'body'
})


module.exports = {
    entry: './app/assets/js',
    output: {
        path: __dirname + '/app/assets/bundle',
        filename: 'bundle.js'
    },
    module: {
        loaders: [
            { test: /\.js$/, loader: 'babel-loader', exclude: /node_modules/},
            { test: /\.sass$/, loader: ['style-loader', 'css-loader', 'sass-loader'], exclude: /node_modules/},
            { test: /\.jpg$/, loader: ['url-loader'], exclude: /node_modules/}
        ]
    },
    plugins: [HtmlWebpackPluginConfig]
}
