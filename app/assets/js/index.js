import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route, IndexRoute, browserHistory, hashHistory } from 'react-router'

import css from '../sass/main.sass'

import Contact from './components/pages/Contact'
import Demos from './components/pages/Demos'
import Home from './components/pages/Home'
import Layout from './components/Layout'
import Login from './components/pages/Login'
import NotFound from './components/pages/NotFound'
import Posts from './components/pages/Posts'
import Signup from './components/pages/Signup'

ReactDOM.render(
    <Router history={browserHistory}>
        <Route path="/" component={Layout}>
            <IndexRoute component={Home}/>
            <Route path="contact" component={Contact}/>
            <Route path="signup" component={Signup}/>
            <Route path="login" component={Login}/>
            <Route path="demos" component={Demos}/>
            <Route path="posts" component={Posts}/>
            <Route path="*" component={NotFound}/>
        </Route>
    </Router>,
    document.getElementById('root'))