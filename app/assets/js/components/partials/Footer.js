import React from 'react'

export default class Footer extends React.Component {
    render() {
        return(
            <footer>
                <p>Maxence Grosjean's portfolio</p>
                <p>Built with ReactJS, HapiJS and MongoDB</p>
            </footer>
        )
    }
}
