import React from 'react'
import { Link } from 'react-router'
import NavLink from '../modules/NavLink'


export default class Header extends React.Component {
    render() {
        return(
            <nav>
                <ul>
                    <li><NavLink to="/">Home</NavLink></li>
                    <li><NavLink to="/contact">Contact</NavLink></li>
                    <li><NavLink to="/signup">Signup</NavLink></li>
                    <li><NavLink to="/login">Login</NavLink></li>
                    <li><NavLink to="/demos">Demos</NavLink></li>
                    <li><NavLink to="/posts">Posts</NavLink></li>
                </ul>
            </nav>
        )
}
}
