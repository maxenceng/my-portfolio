import React from 'react'
import { Link } from 'react-router'

import Footer from '../partials/Footer'

export default class Home extends React.Component {
    render() {
        return(
            <div id="home" className="container">
                <div className="content">
                    <h1>Welcome to my website</h1>
                    <p>Hello, I am a French student really interested in web development.</p>
                    <p>I mostly code in JavaScript.</p>
                    <p>I can also code in PHP if it is required</p>
                    <p>If you have any advice, feel free to use the <Link to="/contact">contact page</Link> to send me a message</p>
                </div>
                <Footer/>
            </div>
        )
    }
}
