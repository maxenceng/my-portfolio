import React from 'react'

export default class Contact extends React.Component {
    render() {
        return(
            <div className="container" id="contact">
                <form action="/contact" method="post" id="form-contact" className="content">
                    <label htmlFor="email">Email</label>
                    <input id="email" type="email" name="email"/>
                    <label htmlFor="message">Message</label>
                    <textarea name="message" id="message" cols="30" rows="10"></textarea>
                    <input type="submit" id="btn-submit" value="Send message"/>
                </form>
            </div>
        )
    }
}

