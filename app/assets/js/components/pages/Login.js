import React from 'react'

export default class Login extends React.Component {
    render() {
        return(
            <div className="container" id="login">
                <form action="/login" method="post" className="content">
                    <label htmlFor="username">Username</label>
                    <input id="username" type="text" name="username"/>
                    <label htmlFor="password">Password</label>
                    <input id="password" type="password" name="password"/>
                    <input type="submit" id="btn-submit" value="Login"/>
                </form>
            </div>
        )
    }
}
