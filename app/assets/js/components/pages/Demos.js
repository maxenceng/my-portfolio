import React from 'react'

export default class Demos extends React.Component {
    render() {
        return(
            <div id="demos">
                <h1>This is the Demos Page</h1>
                    <a href="https://github.com/maxenceng/node-express-mysql">NodeJS-ExpressJS-MySQL</a>
                    <a href="https://github.com/maxenceng/node-express-mongodb">NodeJS-ExpressJS-MongoDB</a>
                    <a href="#">Wordpress site</a>
                    <a href="http://46.101.169.157/">PHP site with a custom framework(school project)</a>
            </div>
        )
    }
}
