import React from 'react'

export default class Signup extends React.Component {
    render() {
        return(
        <div className="container" id="signup">
            <form action="/signup" method="post" className="content">
                <label htmlFor="username">Username</label>
                <input id="username" type="text" name="username"/>
                <label htmlFor="email">Email</label>
                <input id="email" type="email" name="email"/>
                <label htmlFor="password">Password</label>
                <input id="password" type="password" name="password"/>
                <label htmlFor="verify-pass">Verify your password</label>
                <input id="verify-pass" type="password" name="verify-pass"/>
                <input type="submit" id="btn-submit" value="Signup"/>
            </form>
        </div>
        )
    }
}
