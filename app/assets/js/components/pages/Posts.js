import React from 'react'
import axios from 'axios'

export default class Posts extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            posts: []
        }
    }
    componentDidMount() {
        axios.get('/posts/api').then((res) => {
            const posts = res.data
            this.setState({ posts })
        })
    }
    render() {
        return (
            <div id="api">
                <h1>List of posts in the API</h1>
                <p>You can see the API <a href="/posts/api">here</a></p>
                <ul>
                    {this.state.posts.map(post =>
                        <li key={post._id}>{post.title}, created by: {post.created_by}, at: {post.created_at}</li>
                    )}
                </ul>
            </div>
        )
    }
}


