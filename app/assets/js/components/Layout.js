import React from 'react'


import Header from './partials/Header'
import Footer from './partials/Footer'

export default class Layout extends React.Component {
    render() {
        return(
            <div className="container">
                <Header/>
                {this.props.children}
            </div>
        )
    }
}

