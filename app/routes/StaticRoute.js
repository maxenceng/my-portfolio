/**************************************************************
 * DEPENDENCIES
 *************************************************************/

const StaticController = require('../controllers/StaticController')

/**************************************************************
 * ROUTES
 *************************************************************/

const routes = [
    {
        method: 'GET',
        path: '/js/bundle.js',
        handler: StaticController.bundlejs
    },
    {
        method: 'GET',
        path: '/images/index',
        handler: StaticController.imgIndex
    },
    {
        method: 'GET',
        path: '/images/contact',
        handler: StaticController.imgContact
    },
    {
        method: 'GET',
        path: '/images/signup',
        handler: StaticController.imgSignup
    },
    {
        method: 'GET',
        path: '/images/login',
        handler: StaticController.imgLogin
    },
    {
        method: 'GET',
        path: '/{path*}',
        handler: StaticController.index
    }
]

/**************************************************************
 * EXPORTS
 *************************************************************/

module.exports = routes
