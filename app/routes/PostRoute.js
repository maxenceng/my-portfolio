/**************************************************************
 * DEPENDENCIES
 *************************************************************/

const PostController = require('../controllers/PostController')

/**************************************************************
 * ROUTES
 *************************************************************/

const routes = [
    {
        method: 'GET',
        path: '/posts/api',
        handler: PostController.getAll
    },
    {
        method: 'GET',
        path: '/posts/api/{id}',
        handler: PostController.getOne
    },
    {
        method: 'POST',
        path: '/posts/api',
        handler: PostController.create
    },
    {
        method: 'PUT',
        path: '/posts/api/{id}',
        handler: PostController.update
    },
    {
        method: 'DELETE',
        path: '/posts/api/{id}',
        handler: PostController.remove
    }
]

/**************************************************************
 * EXPORTS
 *************************************************************/

module.exports = routes