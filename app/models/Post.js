/**************************************************************
 * DEPENDENCIES
 *************************************************************/

const mongoose = require('mongoose')

/**************************************************************
 * MODEL
 *************************************************************/

const PostSchema = new mongoose.Schema({
    title: { type: String, required: true, unique: true },
    text: { type: String, required: true },
    created_by: { type: String, required: true },
    created_at: { type: Date, default: Date.now }
})

const Post = mongoose.model('Post', PostSchema)

/**************************************************************
 * EXPORTS
 *************************************************************/

module.exports = Post
