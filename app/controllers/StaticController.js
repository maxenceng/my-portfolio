/**************************************************************
 * DEPENDENCIES
 *************************************************************/


/**************************************************************
 * CONTROLLERS
 *************************************************************/



function bundlejs(req, res) {
    return res.file(__dirname + '/../assets/bundle/bundle.js')
}

function imgIndex(req, res) {
    return res.file(__dirname + '/../assets/images/background_home.jpg')
}

function imgContact(req, res) {
    return res.file(__dirname + '/../assets/images/background_contact.jpg')
}

function imgSignup(req, res) {
    return res.file(__dirname + '/../assets/images/background_signup.jpg')
}

function imgLogin(req, res) {
    return res.file(__dirname + '/../assets/images/background_login.jpg')
}

function index(req, res) {
    return res.file(__dirname + '/../views/index.html')
}


/**************************************************************
 * EXPORTS
 *************************************************************/

module.exports = { bundlejs, imgIndex, imgContact, imgSignup, imgLogin, index }
