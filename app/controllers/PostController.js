/**************************************************************
 * DEPENDENCIES
 *************************************************************/

const Post = require('../models/Post')

/**************************************************************
 * CONTROLLERS
 *************************************************************/

function getAll(req, res) {
    Post.find((err, posts) => {
        if(err) {
            return res({
                error: 'Could not get the posts'
            })
        } else {
            return res(posts)
        }
    })
}

function getOne(req, res) {
    Post.findById(
        req.params.id,
        (err, post) => {
            if(err) {
                return res({
                    error: 'Could not get the post'
                }).code(400)
            } else {
                return res(post)
            }
        }
    )
}

function create(req, res) {
    let post = new Post({
        title: req.payload.title,
        text: req.payload.text,
        created_by: req.payload.created_by
    })
    post.save((err) => {
        if(err) {
            return res({
                error: 'Post not created'
            }).code(400)
        } else {
            return res(post).code(201)
        }
    })
}

function update(req, res) {
    if(req.payload.text === "" || req.payload.text === undefined) {
        return res({
            error: 'Post not updated'
        }).code(400)
    } else {
        Post.findOneAndUpdate(
            {_id: req.params.id},
            {$set: {
                text: req.payload.text
            }},
            {upsert: true},
            (err, updatedPost) => {
                if(err) {
                    return res({
                        error: 'Post not updated'
                    }).code(400)
                } else {
                    return res({
                        success: 'Post updated'
                    })
                }
            }
        )
    }
}

function remove(req, res) {
    Post.findOneAndRemove(
        {_id: req.params.id},
        (err, deletedPost) => {
            if(err || deletedPost === null) {
                return res({
                    error: 'Post not deleted'
                }).code(400)
            } else {
                return res({
                    success: 'Post deleted'
                })
            }
        }
    )
}


/**************************************************************
 * EXPORTS
 *************************************************************/

module.exports = { getAll, getOne, create, update, remove }