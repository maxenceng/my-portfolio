const gulp = require('gulp')
const nmon = require('gulp-nodemon')
const sass = require('gulp-sass')
const exec = require('child_process').exec


/**
 * Run `webpack -w` on another terminal for faster changes than with `gulp dev`
 */
gulp.task('default', [
    'nodemon'
])

/**
 * For development on one terminal only.
 * Is slower thant `gulp`+`webpack -w`
 */
gulp.task('dev', [
    'nodemon',
    'sass:watch',
    'webpack:watch'
])

gulp.task('nodemon', () => {
    nmon({script: 'server.js'})
})

gulp.task('sass:watch', () => {
    gulp.watch('app/assets/sass/*.sass', ['webpack'])
})

gulp.task('webpack', (cb) => {
    exec('webpack', (err, stdout, stderr) => {
        console.log(stderr)
        console.log(stdout)
        cb(err)
    })
})

gulp.task('webpack:watch', () => {
    gulp.watch('app/assets/js/**/*.*', ['webpack'])
})