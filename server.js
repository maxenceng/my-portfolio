/**************************************************************
 * DEPENDENCIES
 *************************************************************/

const Hapi = require('hapi')
const mongoose = require('mongoose')
const server = new Hapi.Server()

/**************************************************************
 * DATABASE
 *************************************************************/

mongoose.Promise = global.Promise
mongoose.connect('mongodb://localhost/portfolio')
const db = mongoose.connection
db.on('error', console.error.bind(console, 'conection error:'))

/**************************************************************
 * CONFIG
 *************************************************************/
/*
server.register(require('vision'), (err) => {
    if(err) {
        console.log('Failed to load vision')
    }

    server.views({
        engines: {
            hbs: require('handlebars')
        },
        relativeTo: __dirname,
        path: 'app/views'
    })
})
*/

/**************************************************************
 * CONNECTION
 *************************************************************/

server.connection({
    host: 'localhost',
    port: 8000
})

/**************************************************************
 * INIT ROUTES
 *************************************************************/

const PostRoute = require('./app/routes/PostRoute')
const StaticRoute = require('./app/routes/StaticRoute')

let routes = [...PostRoute, ...StaticRoute]


/**************************************************************
 * ROUTING
 *************************************************************/

server.register(require('inert'), (err) => {
    if(err) {
        console.log('Failed to load inert')
    }

    routes.forEach((route) => {
        server.route(route)
    })
})

/**************************************************************
 * START SERVER
 *************************************************************/

server.start((err) => {
    if(err) {
        throw err
    }
    console.log('Server running at:' + server.info.uri)
})